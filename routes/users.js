const express = require('express');
const router = express.Router();

const controller = require('../controllers/users');
const auth = require('../middleware/auth');

router.post('/uploadPhoto', auth.required, controller.uploadPhoto);
router.post('/changePassword', auth.required, controller.changePassword);
router.post('/register', controller.register);
router.post('/login', controller.login);
router.get('/', auth.required, controller.getCurrentUser);

module.exports = router;