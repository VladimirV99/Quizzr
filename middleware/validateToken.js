const jwt = require('jsonwebtoken');
const User = require('../models/user');
const auth_config = require('../config/auth.config');
module.exports = async (token) => {
    try{
        const decodedToken = await jwt.verify(
            token,
            auth_config.jwt_secret
        );
        const user = await User.findById(decodedToken.user_id);
        if(!user){
            return {success: false};
        }
        return {success: true, user: {id: decodedToken.user_id, username: user.username, photo: user.photo}};
    }catch (err){
        return {success: false};
    }

}
