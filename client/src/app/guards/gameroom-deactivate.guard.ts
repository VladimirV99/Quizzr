import { Injectable } from '@angular/core';
import { CanDeactivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { GameRoomComponent } from '../layout/game-room/game-room.component';

@Injectable({
  providedIn: 'root'
})
export class GameroomDeactivateGuard implements CanDeactivate<unknown> {
  canDeactivate(
    component: GameRoomComponent,
    currentRoute: ActivatedRouteSnapshot,
    currentState: RouterStateSnapshot,
    nextState?: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      if (component.currentGameroom===null || component.gameroom_state_done || confirm("Are you sure you want to leave?")) {
        component.leaveRoom();
        return true
      }
    return false;
  }
  
}
