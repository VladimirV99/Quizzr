import { Component, Input, Output, OnInit, OnChanges, OnDestroy, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-countdown',
  templateUrl: './countdown.component.html',
  styleUrls: ['./countdown.component.scss']
})
export class CountdownComponent implements OnInit, OnChanges, OnDestroy {

  @Input() duration: number;
  @Input() segmentCount: number = 5;
  @Input() running: boolean = false;

  @Output() done = new EventEmitter<boolean>();

  timeLeft: number = 0;
  currentSegmentCount: number;
  segments: Array<number>;

  labelTimer: any;
  progressTimer: any;

  constructor() { }

  ngOnInit(): void { }

  ngOnChanges() {
    if (this.running) {
      this.segments = new Array(this.segmentCount);
      this.timeLeft = this.duration;
      this.currentSegmentCount = this.segmentCount;
      this.startTimers();
    } else {
      this.stopTimers();
    }
  }

  startTimers() {
    this.labelTimer = setInterval(() => {
      this.timeLeft--;
      if(this.timeLeft==0) {
        this.running = false;
        clearInterval(this.labelTimer);
        this.done.emit(true);
      }
    }, 1000);

    this.progressTimer = setInterval(() => {
      this.currentSegmentCount--;
      if(this.currentSegmentCount==0) {
        clearInterval(this.progressTimer);
      }
    }, this.timeLeft/this.segmentCount*1000);
  }

  stopTimers() {
    clearInterval(this.labelTimer);
    clearInterval(this.progressTimer);
  }

  ngOnDestroy() {
    this.stopTimers();
  }

}
