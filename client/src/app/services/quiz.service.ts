import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

import { Quiz } from '../models/quiz';
import { apiUrl } from '../environment';
import { AuthenticationService } from './authentication.service';
import { GameRoom } from '../models/gameRoom';

@Injectable({
  providedIn: 'root'
})
export class QuizService {

  quizUrl = `${apiUrl}/quizzes`;

  constructor(private http: HttpClient, private authService: AuthenticationService) { }

  createOpenTrivia(category: number = 0, amount: number = 10): Observable<Quiz> {
    return this.http.post<Quiz>(`${this.quizUrl}/createOpenTrivia`, {category, amount}, { headers: this.authService.createHeaders() }); 
  }

  createGameroom(id: string, name: string, multiplayer: boolean, isLocked: boolean=false): Observable<any> {
    return this.http.post<any>(`${this.quizUrl}/create-gameroom`, {quiz_id: id, gameroom_name: name, multiplayer, locked: isLocked}, { headers: this.authService.createHeaders() });
  }

  getAllGamerooms(): Observable<GameRoom[]> {
    return this.http.get<GameRoom[]>(`${this.quizUrl}/gamerooms`, { headers: this.authService.createHeaders() });
  }

  getGameRoomById(id: string): Observable<GameRoom> {
    return this.http.get<GameRoom>(`${this.quizUrl}/gamerooms/${id}`, { headers: this.authService.createHeaders() });
  }
  getPrivateGameRoomId(gamecode: number): Observable<any> {
    return this.http.post<any>(`${this.quizUrl}/gamerooms/`, { game_code: gamecode }, { headers: this.authService.createHeaders() });
  }

  getLatestQuizzes(): Observable<Quiz[]> {
    return this.http.get<Quiz[]>(`${this.quizUrl}/latest`, { headers: this.authService.createHeaders() });
  }

  findQuizzes(query: string): Observable<Quiz[]> {
    return this.http.post<Quiz[]>(`${this.quizUrl}/find`, {query}, { headers: this.authService.createHeaders() });
  }

  getQuizDetails(id: string): Observable<Quiz> {
    return this.http.get<Quiz>(`${this.quizUrl}/details/${id}`, { headers: this.authService.createHeaders() });
  }

  getCategories(): Observable<any> {
    return this.http.get<any>(`${this.quizUrl}/openTrivia/categories`, { headers: this.authService.createHeaders() });
  }

  getCategoryName(id: string): Observable<any> {
    return this.http.get<any>(`${this.quizUrl}/openTrivia/category/${id}`, { headers: this.authService.createHeaders() });
  }

  createQuiz(name: string, description: string): Observable<any> {
    return this.http.post<any>(`${this.quizUrl}/create`, {name, description}, { headers: this.authService.createHeaders()});
  }

  getQuizzes(): Observable<any> {
    return this.http.get<any>(`${this.quizUrl}/user`, { headers: this.authService.createHeaders() });
  }

  getQuiz(quiz_id : string): Observable<any> {
    return this.http.get<any>(`${this.quizUrl}/get/${quiz_id}`, { headers: this.authService.createHeaders() });
  }

  addQuestion(question) : Observable<any> {
    return this.http.post<any>(`${this.quizUrl}/add-question`, question, { headers: this.authService.createHeaders() });
  }

  editQuestion(question) : Observable<any> {
    return this.http.post<any>(`${this.quizUrl}/edit-question`, question, { headers: this.authService.createHeaders() });
  }

  deleteQuestion(question_id: string) : Observable<any> {
    return this.http.post<any>(`${this.quizUrl}/delete-question`, {question_id}, { headers: this.authService.createHeaders() });
  }

  reorderQuestions(quiz_id: string, questions: number[]) : Observable<any> {
    return this.http.post<any>(`${this.quizUrl}/reorder-questions`, {quiz_id, questions}, { headers: this.authService.createHeaders() });
  }

  deleteQuiz(quiz_id: string) : Observable<any> {
    return this.http.post<any>(`${this.quizUrl}/delete`, {quiz_id}, { headers: this.authService.createHeaders() });
  }
}
