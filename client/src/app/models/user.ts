export interface User {
    username: string;
    password?: string;
    photo?: string;
    token?: string;
}