import { Quiz } from "./quiz";
import { User } from "./user";

export interface GameRoom {
    _id: string;
    gameroom_name: string;
    author: User;
    quiz: Quiz;
    participants: Array<User>;
    multiplayer: boolean;
    locked: boolean;
    pin?: number;
}