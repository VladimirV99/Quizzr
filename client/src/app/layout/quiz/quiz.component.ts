import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import io from 'socket.io-client';

import { serverUrl } from '../../environment';
import { AuthenticationService } from '../../services/authentication.service';
import { QuizService } from '../../services/quiz.service';
import { SocketEvents } from '../../models/socketEvents';

interface QuestionLog {
  questionModel: string,
  questionData: any,
  userAnswer?: number|number[]|boolean|boolean[],
  correctAnswer?: number|number[]|boolean|boolean[]
}

@Component({
  selector: 'app-quiz',
  templateUrl: './quiz.component.html',
  styleUrls: ['../../form.scss', '../../layout.scss', '../../components/question/question.component.scss', './quiz.component.scss']
})

export class QuizComponent implements OnInit, OnDestroy {

  socket = null;

  rawQuestion: any = null;
  feedback: any = null;

  currentQuestion: any = null;

  isFinished: boolean = false;
  numQuestions: number;
  numCorrect: number = 0;
  numIncorrect: number = 0;

  currentLog: QuestionLog = null;
  questionLogs: Array<QuestionLog> = new Array<QuestionLog>();

  quiz_id: string = this.route.snapshot.params['id'];

  constructor(private route : ActivatedRoute, private quizService: QuizService, private authService: AuthenticationService) { }

  ngOnInit(): void {
    this.socket = io(serverUrl, {query: {token: this.authService.getToken()}});

    this.socket.on(SocketEvents.SINGLEPLAYER_ANSWER_QUESTION, (data) => {
      if(!data.success) {
        console.log(data.message);
      } else {
        // this.questionComponent.evaluateAnswer(data.feedback);
        this.feedback = data.feedback;
        if (!this.feedback.correct)
          this.currentLog.correctAnswer = this.logAnswer(data.feedback.correct_answer);

        this.questionLogs.push(this.currentLog);
        this.currentLog = null;
      }
    });

    this.socket.on(SocketEvents.SINGLEPLAYER_NEXT_QUESTION, (data) => {

      // this.currentQuestion = this.questionComponent.prepareQuestion(data);  
      this.rawQuestion = data.question;    
      if(!data.success) {
        this.currentQuestion = null;
      }
    });

    this.socket.on(SocketEvents.SINGLEPLAYER_FINISHED, (data) => {
      this.isFinished = true;
      this.numCorrect = data.report.num_correct;
      this.numIncorrect = data.report.num_incorrect;
    });

    this.socket.emit(SocketEvents.SINGLEPLAYER_START, { quiz_id: this.quiz_id });
    this.socket.on(SocketEvents.SINGLEPLAYER_START, (data) => {
      console.log(data.message);
    });

  }

  getCurrentQuestion(question: any): void {
    this.currentQuestion = question;

    if (this.currentQuestion != null) {
      this.currentLog = { 
        questionModel: this.currentQuestion.question_model,
        questionData: this.currentQuestion.question_data,
        userAnswer: null,
        correctAnswer: null
      };
    }
  }

  ngOnDestroy() : void {
    this.socket.disconnect();
  }

  mapIndexes(items: any[]) : Array<number> {
    return items.map(item => item.index);
  }

  logAnswer(answer: number|boolean|number[]|boolean[]): number|number[]|boolean|boolean[] {
    let logAnswer: number|number[]|boolean|boolean[];
    switch(this.currentQuestion.question_model) {
    case 'YesNoQuestion':
      logAnswer = answer as boolean;
      break;
    case 'MultipleChoiceQuestion':
      if(this.currentQuestion.question_data.multiple_correct)
        logAnswer = answer as boolean[];
      else
        logAnswer = answer as number;
      break;
    case 'DragDropQuestion':
        logAnswer = answer as number[];
      break;
    default:
      console.log('Unknown question type');
      break;
    }
    return logAnswer;
  }

  arrayEquals(a1: any[], a2: any[]) {
    return (a1.length==a2.length) && a1.every((x, i) => { return a2[i]===x });
  }

  answerQuestion(answer: number|boolean|number[]|boolean[]): void {
    this.currentLog.userAnswer = this.logAnswer(answer);

    this.socket.emit(SocketEvents.SINGLEPLAYER_ANSWER_QUESTION, {answer});
  }
}
