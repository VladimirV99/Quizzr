import { Component, ElementRef, HostListener, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import * as io from 'socket.io-client'
import { Question } from 'src/app/models/question';

import { GameRoom } from '../../models/gameRoom';
import { SocketEvents } from '../../models/socketEvents';
import { User } from '../../models/user';
import { AuthenticationService } from '../../services/authentication.service';
import { QuizService } from '../../services/quiz.service';
import { UiService } from '../../services/ui.service';

@Component({
  selector: 'app-game-room',
  templateUrl: './game-room.component.html',
  styleUrls: ['../../layout.scss', '../../form.scss', './game-room.component.scss']
})
export class GameRoomComponent implements OnInit, OnDestroy {

  @ViewChild('messageWrap') messageWrap: ElementRef;
  @ViewChild('message') chatInput: ElementRef;
  @ViewChild('emojiPicker') emojiPicker: ElementRef;

  socket = io.io('http://localhost:3000', { query: {token: this.authService.getToken()} });

  roomId: string;
  roomCode: string;
  roomPIN: string;
  participants: User[] = [];
  isHost: boolean = false;
  gameroom_state_idle: boolean = false;

  currentGameroom: GameRoom = null;

  messages:any = new Array<any>();

  nameColors: Map<string, string> = new Map<string, string>();

  currentQuestion: any = null;
  rawQuestion: any = null;
  feedback: any = null;

  public gameroom_state_done: boolean = false;
  ranking: any[] = [];

  emojiPickerVisible: boolean = false;
  dialogVisible: boolean = false;

  constructor(private quizService: QuizService, public authService: AuthenticationService, private route: ActivatedRoute, private router: Router, public uiService: UiService) { }

  ngOnInit(): void {
    this.roomId = this.route.snapshot.params["gameroom-id"];
    if (this.route.snapshot.queryParamMap.has('pin')) {
      this.roomPIN = this.route.snapshot.queryParams['pin'];
      this.roomCode = this.route.snapshot.queryParams['gamecode'];
      this.socket.emit(SocketEvents.GAMEROOM_QUIZ_JOIN, { gameroom_id: this.roomId, pin: this.roomPIN });
    } else {
      this.socket.emit(SocketEvents.GAMEROOM_QUIZ_JOIN, { gameroom_id: this.roomId });
    }

    this.socket.on(SocketEvents.GAMEROOM_QUIZ_JOIN, (data) => {
      if(!data.success) {
        console.error(data.message);
        this.router.navigate(['/home']);
      } else {
        this.quizService.getGameRoomById(this.roomId).subscribe(res => {
          this.currentGameroom = res;

          this.participants = res.participants;

          for (let user of this.participants) {
            if (user.username !== this.authService.user.username) {
              this.nameColors.set(user.username, '#' + Math.floor(Math.random()*16777215).toString(16));
            }
          }

          if (res.author.username == this.authService.user.username) {
            this.isHost = true;
            this.dialogVisible = true;
          }
        });
      }
    });

    this.socket.on(SocketEvents.GAMEROOM_QUIZ_CHAT_MSG_META_RECV, (data) => {
      this.receiveMessage(data.chat_message, 'meta', '');

      if (data.hasOwnProperty('user')) {
        this.participants.push(data.user);
        if (data.user.username !== this.authService.user.username && !this.nameColors.has(data.user.username)) {
          this.nameColors.set(data.user.username, '#' + Math.floor(Math.random()*16777215).toString(16));
      }
    }
    });

    this.socket.on(SocketEvents.GAMEROOM_QUIZ_CHAT_MSG_RECV, (data) => {
      this.receiveMessage(data.chat_message, data.username, data.time);
    });

    this.socket.on(SocketEvents.GAMEROOM_QUIZ_LEAVE, (data) => {
      this.participants = this.participants.filter(val => { return val.username != data.user.username; });
    });

    this.socket.on(SocketEvents.GAMEROOM_QUIZ_NEXT_QUESTION, (data) => {
      if (!this.gameroom_state_idle) {
        this.gameroom_state_idle = true;
      }
      this.rawQuestion = data;
    });

    this.socket.on(SocketEvents.GAMEROOM_QUIZ_QUESTION_FEEDBACK, (data) => {
      this.feedback = data.feedback;
    });

    this.socket.on(SocketEvents.GAMEROOM_QUIZ_FINISHED, (data) => {
      this.gameroom_state_done = true;
      this.ranking = data.rankings;
      this.receiveMessage(data.message, 'meta', '');  
    });

    this.socket.on(SocketEvents.GAMEROOM_QUIZ_DESTROYED, (data) => {
      this.router.navigate(['/']);
    });
  }

  sendChatMessage(message: string): void {
    if(message === '')
      return;
    this.socket.emit(SocketEvents.GAMEROOM_QUIZ_CHAT_MSG_SEND, { gameroom_id: this.roomId, chat_message: message });
  }

  receiveMessage(message: string, from: string, timestamp: string): void {
    if(message == '')
      return;
    let time = timestamp.substr(0, timestamp.lastIndexOf(':'));
    this.messages.push({msg: message, from: from, timestamp: time});
  }

  getCurrentQuestion(question: any): void {
    this.currentQuestion = question;
  }

  scrollToBottom(): void {
    this.messageWrap.nativeElement.scrollTop = this.messageWrap.nativeElement.scrollHeight;
  }

  leaveRoom(): void {
    this.socket.emit(SocketEvents.GAMEROOM_QUIZ_LEAVE, { gameroom_id: this.roomId });
  }

  startQuiz(): void {
    this.socket.emit(SocketEvents.GAMEROOM_QUIZ_START, { gameroom_id: this.roomId });
    this.gameroom_state_idle = true;
  }

  answerQuestion(answer: any): void {
    this.socket.emit(SocketEvents.GAMEROOM_QUIZ_ANSWER_QUESTION, { gameroom_id: this.roomId, answer: answer })
  }

  ngOnDestroy(): void {
    this.socket.disconnect();
  }

  toggleEmojiPicker($event: Event) {
    $event.stopPropagation();
    $event.preventDefault();
    this.emojiPickerVisible = !this.emojiPickerVisible;
  }

  @HostListener('document:click', ['$event']) clickOutside($event: Event) {
    let currentElement = $event.target;

    if (this.emojiPickerVisible) {
      while (currentElement['parentNode'] != null) {
        if (currentElement['className'] === 'emoji-mart' )
          return;
        currentElement = currentElement['parentNode'];
      }
  }
    this.emojiPickerVisible = false;
  }

  addEmoji($event: Event): void {
    this.chatInput.nativeElement.value = `${this.chatInput.nativeElement.value}`+`${$event['emoji'].native}`;
  }

}
