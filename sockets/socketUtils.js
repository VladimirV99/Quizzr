const GameRoom = require('../models/gameroom');

module.exports.errorFunction = (socket, eventName) => {
    return (message) => {
        socket.emit(eventName, {success: false, message});
    }
};

module.exports.justSuccessFunction = (socket, eventName) => {
    return (message) => {
        socket.emit(eventName, {success: true, message});
    }
};

// TODO: Too lazy to refactor every `socket.emit(event, {success: true, blabla})` to this, do it eventually
module.exports.dataFunction = (socket, eventName) => {
    return (message, data) => {
        socket.emit(eventName, {success: true, message: message, ...data});
    }
}

module.exports.isUserAlreadyInGame = (socket, io) => {
    for (const [, socketObj] of io.sockets.sockets){
        if (socketObj.user.id == socket.user.id
            && socketObj.hasOwnProperty('inGame')
            && socketObj.inGame){
                return true;
        }
    }
    return false;
}

module.exports.emitToRoom = (socket, io, data, eventName) => {
    if (!data.hasOwnProperty('gameroom_id')){
        return {success: false, message: 'No game id specified.'};
    }
    
    if (!socket.rooms.has(data.gameroom_id)){
        return {success: false, message: `You're not in this room.`}
    }
    io.in(data.gameroom_id).emit(eventName, data);
    return {success: true};
}

module.exports.prepareChatMessage = (socket, data) => {
    if (!data.hasOwnProperty('chat_message')){
        return {success: false, message: `No text.`};
    }
    if (!data.hasOwnProperty('gameroom_id')){
        return {success: false, message: `No game id.`};
    }
    newData = {...data};
    newData.username = socket.user.username;
    newData.time = new Date().toLocaleTimeString('en-GB');
    return {success: true, data: newData};
}

module.exports.userDisconnect = (socket, io) => {
    console.log(`TODO: userDisconnect, delete opentrivia if any, leave gameroom if any, delete gameroom if admin (or give admin to someone else, not sure yet but nvm)`);
    
}