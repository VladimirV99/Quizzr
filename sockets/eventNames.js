module.exports.SINGLEPLAYER_START                      = 'singleplayer_start';
module.exports.SINGLEPLAYER_NEXT_QUESTION              = 'singleplayer_next_question';
module.exports.SINGLEPLAYER_ANSWER_QUESTION            = 'singleplayer_answer_question';
module.exports.SINGLEPLAYER_FINISHED                   = 'singleplayer_finished';

module.exports.GAMEROOM_QUIZ_CREATE                = 'group_quiz_create';
module.exports.GAMEROOM_QUIZ_JOIN                  = 'group_quiz_join';
module.exports.GAMEROOM_QUIZ_LEAVE                 = 'group_quiz_leave';
module.exports.GAMEROOM_QUIZ_CHAT_MSG_SEND         = 'group_quiz_chat_msg_send';
module.exports.GAMEROOM_QUIZ_CHAT_MSG_RECV         = 'group_quiz_chat_msg_recv';
module.exports.GAMEROOM_QUIZ_CHAT_MSG_META_RECV    = 'group_quiz_chat_msg_meta_recv';
module.exports.GAMEROOM_QUIZ_START                 = 'gameroom_quiz_start';
module.exports.GAMEROOM_QUIZ_NEXT_QUESTION         = 'gameroom_quiz_next_question';
module.exports.GAMEROOM_QUIZ_ANSWER_QUESTION       = 'gameroom_quiz_answer_question';
module.exports.GAMEROOM_QUIZ_QUESTION_FEEDBACK     = 'gameroom_quiz_question_feedback';
module.exports.GAMEROOM_QUIZ_FINISHED              = 'gameroom_quiz_question_finished';
module.exports.GAMEROOM_QUIZ_DESTROYED             = 'gameroom_quiz_destroyed';